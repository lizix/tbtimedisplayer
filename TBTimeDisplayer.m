//
//  TBTimeDisplayer.m
//  TimeCalculator
//
//  Created by Zixuan Li on 11/28/13.
//  Copyright (c) 2013 Zixuan Li. All rights reserved.
//

#import "TBTimeDisplayer.h"

@implementation TBTimeDisplayer

+ (id)sharedInstance {
    
    static TBTimeDisplayer *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        __sharedInstance = [[TBTimeDisplayer alloc] init];
        
    });
    
    return __sharedInstance;
    
}

- (NSString *)getTextDate:(NSDate *)originalTime {
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger unitFlags =  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags
                                                fromDate:originalTime
                                                  toDate:[NSDate date] options:0];
    
    NSInteger days = [components day];
    NSInteger hours = [components hour];
    NSInteger minutes = [components minute];
    NSInteger seconds = [components second];
    
    NSString *dateAsText = [[NSString alloc] init];
    
    if (days > 0) {
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        timeFormatter.dateFormat = @"HH:mm";
        NSString *timeString = [timeFormatter stringFromDate: originalTime];
        
        NSString *numDay = [[NSString alloc] init];
        if (days > 1) {
            
            numDay = @"days";
            dateAsText = [NSString stringWithFormat:@"%ld %@ ago at %@", (long)days, numDay, timeString];

            
        } else {
            
            dateAsText = [NSString stringWithFormat:@"Yesterday at %@", timeString];
            
        }
        
        return dateAsText;
        
    } else if (hours > 0) {
        
        NSString *numHour = nil;
        if (hours > 1) {
            
            numHour = @"hours";
            
        } else {
            
            numHour = @"hour";
            
        }
        
        dateAsText = [NSString stringWithFormat:@"%ld %@ ago", (long)hours, numHour];
        return dateAsText;
        
    } else if (minutes > 0) {
        
        NSString *numMinute = nil;
        if (minutes > 1) {
            
            numMinute = @"minutes";
            
        } else {
            
            numMinute = @"minute";
            
        }
        
        dateAsText = [NSString stringWithFormat:@"%ld %@ ago", (long)minutes, numMinute];
        return dateAsText;
        
    } else if (seconds > 0) {
        
        NSString *numSecond = nil;
        if (seconds > 1) {
            
            numSecond = @"seconds";
            
        } else {
            
            numSecond = @"second";
            
        }
        
        dateAsText = [NSString stringWithFormat:@"%ld %@ ago", (long)seconds, numSecond];
        return dateAsText;
        
    } else {
        
        dateAsText = @"Just now";
        return dateAsText;
        
    }
    
}


@end
