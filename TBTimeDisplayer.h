//
//  TBTimeDisplayer.h
//  TimeCalculator
//
//  Created by Zixuan Li on 11/28/13.
//  Copyright (c) 2013 Zixuan Li. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBTimeDisplayer : NSObject

+ (id)sharedInstance;

/**
 *get text representation of time: e.g. 2 days. yesterday.
 *@param NSDate originalTime
 *@return NSString dateAsText
 */
- (NSString *)getTextDate:(NSDate *)originalTime;

@end
